#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto

DEB_BUILD_ARCH ?= $(shell dpkg-architecture -qDEB_BUILD_ARCH)

# set installation prefix
export PREFIX=/usr/

# turn of parallel builds here to make build deterministic
export PARALLEL=1
export PARALLEL_NJOBS=1

# some archs require turning of some optimizations
NOEXPOPT_ARCHS=arm64 ppc64el s390x ia64 powerpc ppc64 riscv64

# turn off hanging tests on i386
ifeq ($(DEB_BUILD_ARCH), i386)
export ODE_SIMU_OFF=1
endif

ifneq (,$(filter $(NOEXPOPT_ARCHS), $(DEB_BUILD_ARCH)))
export DEB_CFLAGS_MAINT_APPEND = -fno-expensive-optimizations
endif

# Some tests fail on the following architectures probably
# due to minor differences in floating point processing.
# For now, just turn it off...
NOTEST_ARCHS=hurd-i386 kfreebsd-i386

# For this architesture the test will be run but it will pass in any case even when errors occure
PRINT_ERRORS_WHEN_TESTING=alpha sh4 arc m68k

%:
	dh $@

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifeq (,$(filter $(NOTEST_ARCHS), $(DEB_BUILD_ARCH)))
ifeq (,$(filter $(PRINT_ERRORS_WHEN_TESTING), $(DEB_BUILD_ARCH)))
	make utest
else
	# the test will be run but its passing in any case
	make utest || true
endif
endif
endif
